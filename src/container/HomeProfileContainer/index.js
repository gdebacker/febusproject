// @flow
import * as React from "react";
import { connect } from "react-redux";
import HomeProfile from "../../stories/screens/HomeProfile";
import datas from "./data";
import { fetchList } from "./actions";
export interface Props {
	navigation: any,
	fetchList: Function,
	data: Object,
}
export interface State {}
class HomeProfileContainer extends React.Component<Props, State> {
	componentDidMount() {
		this.props.fetchList(datas);
	}
	render() {
		return <HomeProfile navigation={this.props.navigation} list={this.props.data} />;
	}
}

function bindAction(dispatch) {
	return {
		fetchList: url => dispatch(fetchList(url)),
	};
}

const mapStateToProps = state => ({
	data: state.homeProfileReducer.list,
	isLoading: state.homeProfileReducer.isLoading,
});
export default connect(mapStateToProps, bindAction)(HomeProfileContainer);
