import storage from 'redux-persist/lib/storage';
import { persistReducer } from 'redux-persist';

const initialState = {
	data: [],
	accountObject: null,
	isLoading: true,
    isLogged: false,
    step:0,
    pseudo:"",
    nom:"",
    prenom:"",
    naissance:"",
    tel:"",
    sexe:"",
    domaine:"",
    style:""
};

export default function(state: any = initialState, action: Function) {
    if (action.type === "CHANGE_STEP") {
        return {
            ...state,
            step: action.data.step,
            accountObject: action.data
        };
    }
	if (action.type === "FETCH_DATA_SUCCESS") {
		return {
			...state,
			data: action.data,
		};
	}
    if (action.type === "SAVE_ACCOUNT") {
        return {
            ...state,
        };
    }
    if (action.type === "SAVE_ACCOUNT_SUCCESS") {
        return {
            ...state,
            accountObject: action.data,
            isLogged: true
        };
    }
    if (action.type === "SAVE_ACCOUNT_FAIL") {
        return {
            ...state
        };
    }
	if (action.type === "DATA_IS_LOADING") {
		return {
			...state,
			isLoading: action.isLoading,
		};
	}
	return state;
}



/*
import storage from 'redux-persist/lib/storage';
import { persistReducer } from 'redux-persist';

const initialState = {
    data: [],
    accountObject: null,
    isLoading: true,
};

const loginReducer = (state: any = initialState, action: Function) => {
    if (action.type === "FETCH_DATA_SUCCESS") {
        return {
            ...state,
            data: action.data,
        };
    }
    if (action.type === "SAVE_ACCOUNT") {
        return {
            ...state,
        };
    }
    if (action.type === "SAVE_ACCOUNT_SUCCESS") {
        console.log("saving account reducer");
        console.log(action.data);
        return {
            ...state,
            accountObject: action.data,
        };
    }
    if (action.type === "SAVE_ACCOUNT_FAIL") {
        return {
            ...state
        };
    }
    if (action.type === "DATA_IS_LOADING") {
        return {
            ...state,
            isLoading: action.isLoading,
        };
    }
    return state;
}

const persistConfig = {
    key: 'auth',
    storage: storage,
    blacklist: ['isLoading']
};ù/

export default persistReducer(persistConfig, loginReducer);

*/
