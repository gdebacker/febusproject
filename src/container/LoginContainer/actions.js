import {AsyncStorage} from 'react-native';

export function dataIsLoading(bool: boolean) {
	return {
		type: "DATA_IS_LOADING",
		isLoading: bool,
	};
}
export function fetchDataSuccess(data: Object) {
	return {
		type: "FETCH_DATA_SUCCESS",
		data,
	};
}
export function fetchData(url: any) {
    return dispatch => {
        dispatch(fetchDataSuccess((url: any)));
        dispatch(dataIsLoading(false));
    };
}

export function saveAccountDataSuccess(data: Object) {
    return {
        type: "SAVE_ACCOUNT_SUCCESS",
        data,
    };
}

export function saveAccountDataError(error: Object) {
    return {
        type: "SAVE_ACCOUNT_FAIL",
        error,
    };
}

/*export function saveAccountDatas(data:any) {
    console.log("action save account")
    return function action(dispatch) {
        dispatch({ type: "SAVE_ACCOUNT" });
        const request = async () => {
            console.log("gfdgfdgfdgfdgfdgfd");
            console.log(data);
            try {
                await AsyncStorage.setItem('account', JSON.stringify(data));
            } catch (error) {
                // Error saving data
                console.log("error saving user object");
                console.log(error);
                dispatch(saveAccountDataError((error: any)));
            }
        }

        return dispatch => {
            dispatch(saveAccountDataSuccess((data: any)));
        };
    }

}*/


export function saveAccountDatas(data:any) {
    console.log("action save account");
    return function action(dispatch) {
        try {
            AsyncStorage.setItem('account', JSON.stringify(data));
            dispatch(saveAccountDataSuccess((data)));

        } catch (error) {
            // Error saving data
            console.log("error saving user object");
            console.log(error);
            dispatch(saveAccountDataError((error: any)));
        }
    }

}

export function changeStepSuccess(data: Object) {
    return {
        type: "CHANGE_STEP",
        data,
    };
}

export function changeStep(data: any) {
    return dispatch => {
        dispatch(changeStepSuccess((data: any)));
    };
}
