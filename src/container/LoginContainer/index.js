// @flow
import * as React from "react";
import { Item, Input, Icon, Toast, Form, Label } from "native-base";
import { Field, reduxForm } from "redux-form";
import Login from "../../stories/screens/Login";
import DomaineSelection from "../../stories/screens/Login/DomaineSelection";
import {AsyncStorage} from "react-native";
import datas from "./data";
import { fetchData, saveAccountDatas, changeStep} from "./actions";
import { connect } from "react-redux";
import AvatarSelection from "../../stories/screens/Login/AvatarSelection";

export interface Props {
    navigation: any;
    data:Object;
    isLoading:any;
    isLogged:any;
    accountObject:any;
    step:any;
}
export interface State {}
class LoginForm extends React.Component<Props, State> {

    constructor(props){
        super(props);
        console.log("constructor login");
        try {
            AsyncStorage.getItem('account')
                .then(json => {
                    const user = JSON.parse(json);
                    console.log("***************** USER FROM ASYNC STORAGE (loginContainer) *******************");
                    console.log(user);
                    if(user != null) {
                        if (user.username !== null) {
                            this.props.navigation.navigate("HomeProfile");
                        }
                    }
                })
        } catch (error) {
            console.log("******************error getting user (loginContainer)********************");
            console.log(error);
        }

    }

    componentWillReceiveProps(nextProps){
        if(nextProps.isLogged){
            this.props.navigation.navigate("HomeProfile");
        }
        console.log("loginContainer/index.js:componentWillReceiveProps()");
        console.log(nextProps);
    }

    componentDidMount() {
        this.props.fetchData(datas);
    }

    changeStep = (userObject, direction) => {
        var changeStep = false;
        console.log("loginContainer/index.js:changeStep() : triggered");
        if(direction == "back"){
            console.log("loginContainer/index.js:changeStep() : navigation <-")
            if(userObject.step != 1){
                userObject.step = userObject.step -1;
                console.log(userObject);
                this.props.changeStep(userObject);
            }
        }
        else {


            //console.log(this.props.testState);
            changeStep = true;

            //console.log(this.props);
            console.log(userObject);
            switch (userObject.step) {
                case 1:
                    if (
                        userObject.username == "" ||
                        userObject.lastname == "" ||
                        userObject.firstname == "" ||
                        userObject.birth == "" ||
                        userObject.phone == "" ||
                        userObject.gender == ""
                    ) {
                        Toast.show({
                            text: "Il faut renseigner toutes les infomartions",
                            duration: 2000,
                            position: "bottom",
                            textStyle: {textAlign: "center"}
                        });
                        changeStep = false;
                    }
                    break;
                case 2:
                    if (userObject.domain == "" || userObject.style == "") {
                        Toast.show({
                            text: "Il faut renseigner chaque choix artistique",
                            duration: 2000,
                            position: "bottom",
                            textStyle: {textAlign: "center"}
                        });
                        changeStep = false;
                    }
                    break;
                case 3:
                    console.log("step 3");
                    //implement avatar selection conditions
                    break;
                default:

                    break;
            }
            if (changeStep) {
                if(userObject.step === 3){
                    this.props.saveAccountDatas(userObject);
                }
                else{
                    console.log("entering change step action");
                    userObject.step = userObject.step + 1;
                    console.log("********************** changeStep will be triggered (loginContainer -> redux action changeStep()) *************************");
                    this.props.changeStep(userObject);
                }
            }
        }
    }

    render() {
        const step = this.props.step;
        var screenToDisplay = <Login accountObjectState={this.props.accountObject} data={this.props.data} navigation={this.props.navigation} onChangeStep={(userObject,direction) => this.changeStep(userObject,direction)}/>;
        switch (step) {
            case 1:
                return(
                    <Login accountObjectState={this.props.accountObject} data={this.props.data} navigation={this.props.navigation} onChangeStep={(userObject,direction) => this.changeStep(userObject,direction)}/>
                )
                break;
            case 2:
                return(
                    <DomaineSelection data={this.props.data} userObject={this.props.accountObject} navigation={this.props.navigation} onChangeStep={(userObject,direction) => this.changeStep(userObject,direction)}/>
                )
                break;
            case 3:
                return(
                    <AvatarSelection data={this.props.data} userObject={this.props.accountObject} navigation={this.props.navigation} onChangeStep={(userObject,direction) => this.changeStep(userObject,direction)}/>
                )
                break;
            default:
                return(
                    <Login accountObjectState={this.props.accountObject} data={this.props.data} navigation={this.props.navigation} onChangeStep={(userObject,direction) => this.changeStep(userObject,direction)}/>
                )
                break;
        }
        return (
            <Login accountObjectState={this.props.accountObject} data={this.props.data} navigation={this.props.navigation} onChangeStep={(userObject,direction) => this.changeStep(userObject,direction)}/>
        );
    }
}

function bindAction(dispatch) {
    return {
        fetchData: url => dispatch(fetchData(url)),
        saveAccountDatas: accountDatas => dispatch(saveAccountDatas(accountDatas)),
        changeStep: accountDatas => dispatch(changeStep(accountDatas)),
    };
}

const mapStateToProps = state => ({
    //testState : state,
    accountObject: state.loginReducer.accountObject,
    step: state.loginReducer.step,
    data: state.loginReducer.data,
    isLoading: state.loginReducer.isLoading,
    isLogged: state.loginReducer.isLogged
});
export default connect(mapStateToProps, bindAction)(LoginForm);
