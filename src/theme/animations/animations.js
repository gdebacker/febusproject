let MyTransition = (index, position) => {
    const inputRange = [index - 1, index, index + 1];
    const outputRange = [.8, 1, 1];
    const opacity = position.interpolate({
        inputRange,
        outputRange,
    });

    const scaleY = position.interpolate({
        inputRange,
        outputRange,
    });

    return {
        opacity,
        transform: [
            {scaleY}
        ]
    };
};

let MyCustomTransitionLeft = (index, position) => {
    const inputRange = [index - 1, index, index + 0.99, index + 1];

    const opacity = position.interpolate({
        inputRange,
        outputRange: ([0, 1, 1, 0]),
    });

    const translateY = 0;
    const translateX = position.interpolate({
        inputRange,
        outputRange: ([-50, 0, 0, 0]),
    });

    return {
        opacity,
        transform: [
            { translateX },
            { translateY }
        ],
    };
};

let MyCustomTransitionRight = (index, position) => {
    const inputRange = [index - 1, index, index + 0.99, index + 1];

    const opacity = position.interpolate({
        inputRange,
        outputRange: ([0, 1, 1, 0]),
    });

    const translateY = 0;
    const translateX = position.interpolate({
        inputRange,
        outputRange: ([50, 0, 0, 0]),
    });

    return {
        opacity,
        transform: [
            { translateX },
            { translateY }
        ],
    };
};

export const TransitionConfiguration = () => {
    return {
        // Define scene interpolation, eq. custom transition
        screenInterpolator: (sceneProps) => {

            const {position, scene} = sceneProps;
            const {index, route} = scene;
            const params = route.params || {};
            const transition = params.transition || 'default';

            return {
                myCustomTransitionLeft: MyCustomTransitionLeft(index, position),
                myCustomTransitionRight: MyCustomTransitionRight(index, position),
                default: MyTransition(index, position),
            }[transition];
        }
    }
};
