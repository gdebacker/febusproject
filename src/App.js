// @flow
import React from "react";
import { StackNavigator, DrawerNavigator } from "react-navigation";
import { Root } from "native-base";
import Login from "./container/LoginContainer";
import Home from "./container/HomeContainer";
import HomeProfile from "./container/HomeProfileContainer";
import BlankPage from "./container/BlankPageContainer";
import Sidebar from "./container/SidebarContainer";
import {TransitionConfiguration} from "./theme/animations/animations";
import {AsyncStorage} from 'react-native';
import StartYourCareer from "./stories/screens/StartYourCareer";
import ChooseHowToStartYourCareer from "./stories/screens/ChooseHowToStartYourCareer";
import CoverLandAddCover from "./stories/screens/CoverLand/CoverLandAddCover"
import DefiPage from "./stories/screens/DefiPage"
import FaceRecognition from "./stories/screens/FaceRecognition"
import DefiDual from "./stories/screens/DefiDual";
import ResultPage from "./stories/screens/ResultPage";
import StartPage from "./stories/screens/StartPage"

const Drawer = DrawerNavigator(
    {
        HomeProfile: { screen: HomeProfile },
    },
    {
        initialRouteName: "HomeProfile",
        contentComponent: props => <Sidebar {...props} />,
    }
);

var App1 = StackNavigator(
    {
        FaceRecognition:{screen:FaceRecognition},
        Login: { screen: Login },
        BlankPage: { screen: BlankPage },
        Drawer: { screen: Drawer },
        Home: {screen: Home},
        HomeProfile: {screen: HomeProfile},
        StartYourCareer : {screen: StartYourCareer},
        ChooseHowStartYourCareer: {screen: ChooseHowToStartYourCareer},
        CoverLandAddCover:{screen: CoverLandAddCover},
        DefiPage:{screen: DefiPage},
        DefiDual:{screen:DefiDual},
        StartPage:{screen:StartPage},
        ResultPage:{screen: ResultPage}
    },
    {
        initialRouteName: "StartPage",
        headerMode: "none",
        transitionConfig: TransitionConfiguration
    }
);

export default () => (
    <Root>
        <App1/>
    </Root>
);
