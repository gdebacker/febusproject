import * as React from "react";
import { Container, Header, Title, Content, Text, Button, Icon, Left, Right, Body } from "native-base";

import styles from "./styles";
import {PanResponder, Dimensions, View} from "react-native";
export interface Props {
	navigation: any;
}
export interface State {}

const { width, height } = Dimensions.get("window");

const getDirectionAndColor = ({ moveX, moveY, dx, dy }) => {
    const draggedDown = dy > 30;
    const draggedUp = dy < -30;
    const draggedLeft = dx < -30;
    const draggedRight = dx > 30;
    const isRed = moveY < 90 && moveY > 40 && moveX > 0 && moveX < width;
    const isBlue = moveY > height - 50 && moveX > 0 && moveX < width;
    let dragDirection = "";

    if (draggedDown || draggedUp) {
        if (draggedDown) dragDirection += "dragged down";
        if (draggedUp) dragDirection += "dragged up";
    }

    if (draggedLeft || draggedRight) {
        if (draggedLeft) dragDirection = "dragged left";
        if (draggedRight) dragDirection = "dragged right";
    }

    if (isRed) return `red ${dragDirection}`;
    if (isBlue) return `blue ${dragDirection}`;
    if (dragDirection) return dragDirection;
};

class BlankPage extends React.Component<Props, State> {

    interacted : false;

    componentWillMount() {
        this._panResponder = PanResponder.create({
            onMoveShouldSetPanResponder: (evt, gestureState) => {
                if(gestureState.dy !== 0 || gestureState.dx !== 0){return true;} else{return false;}
            },
            onPanResponderMove: (evt, gestureState) => {
                // The most recent move distance is gestureState.move{X,Y}
                const drag = getDirectionAndColor(gestureState);
                if ( drag === "dragged left" && !this.interacted) {
                    this.interacted = true;
                    this.props.navigation.goBack();
                    setTimeout(() => {this.interacted = false}, 1000)
                }
            },

        });
    }

	render() {
		const param = this.props.navigation.state.params;
		return (
			<Container {...this._panResponder.panHandlers} style={styles.container}>
                <Header>

                    <Body style={{ flex: 3 }}>
                    <Title>{param ? param.name.item : "Blank Page"}</Title>
                    </Body>
                    <Right />
                </Header>

				<Content padder>
                    <Text>Only to fill</Text>
					<Text>{param !== undefined ? param.name.item : "Create Something Awesome . . ."}</Text>
				</Content>
			</Container>
		);
	}
}

export default BlankPage;
