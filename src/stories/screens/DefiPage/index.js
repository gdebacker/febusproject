import * as React from "react";
import {
    Container,
    Header,
    Title,
    Content,
    Text,
    Button,
    Icon,
    Left,
    Body,
    Card,
    CardItem,
    Right,
    List,
    ListItem,
    Label,
    Item,
    Input
} from "native-base";
import { Col, Row, Grid } from "react-native-easy-grid";
import LinearGradient from 'react-native-linear-gradient'
import { RNCamera } from 'react-native-camera';
//import Sound from 'react-native-sound';
//import styles from "./styles";
import { Dimensions, View, StyleSheet, TouchableOpacity, Image, PanResponder } from "react-native";
import SwitchButton from "../../../theme/components/SwitchButton";
//import Voice from 'react-native-voice';
//import NewCover from './NewCover';
export interface Props {
    navigation: any;
    list: any;
}
export interface State { }
const { width, height } = Dimensions.get("window");
const getDirectionAndColor = ({ moveX, moveY, dx, dy }) => {
    const draggedDown = dy > 30;
    const draggedUp = dy < -30;
    const draggedLeft = dx < -30;
    const draggedRight = dx > 30;
    const isRed = moveY < 90 && moveY > 40 && moveX > 0 && moveX < width;
    const isBlue = moveY > height - 50 && moveX > 0 && moveX < width;
    let dragDirection = "";

    if (draggedDown || draggedUp) {
        if (draggedDown) dragDirection += "dragged down";
        if (draggedUp) dragDirection += "dragged up";
    }

    if (draggedLeft || draggedRight) {
        if (draggedLeft) dragDirection = "dragged left";
        if (draggedRight) dragDirection = "dragged right";
    }

    if (isRed) return `red ${dragDirection}`;
    if (isBlue) return `blue ${dragDirection}`;
    if (dragDirection) return dragDirection;
};

class DefiPage extends React.Component<Props, State> {
    sound: any;
    constructor(props) {
        super(props);
        this.state = {
            finished: false,
            defis: {}
        }
    }
    componentWillMount() {

        let options = {
            headers: {
                'Authorization': "Basic YWRtaW46QGRtaW5SM2YxbmY=",
                'x-client-id': '2',
                'x-pass': '9pbLfyk0pyFy',
            },
            method: 'GET'
        };

        fetch('http://test.gauthierdebacker.net/public/getAllDefi4User', options)
        .then(response => {
            return response.json()
                .then(responseJson => {
                    console.log(responseJson.user);
                    this.setState({
                        defis: responseJson.user
                    })
                    return responseJson;
                });
        });

        this._panResponder = PanResponder.create({
            onMoveShouldSetPanResponder: (evt, gestureState) => {
                if(gestureState.dy !== 0 || gestureState.dx !== 0){return true;} else{return false;}
            },
            onPanResponderMove: (evt, gestureState) => {
                // The most recent move distance is gestureState.move{X,Y}
                const drag = getDirectionAndColor(gestureState);
                if ( drag === "dragged left" && !this.interacted) {
                    this.interacted = true;
                    this.props.navigation.goBack();
                    setTimeout(() => {this.interacted = false}, 1000)
                }
            },

        });
        
    }
    componentDidMount = () =>{
        setTimeout(() => {this.setState({finished: true})}, 2000)
    }

    render() {
        const finished = this.state.finished;
        const defis = this.state.defis;
        
        return (
            <View {...this._panResponder.panHandlers} style={styles.container}>

            <View style={{height:"20%"}}>
               <Text style={{position:'absolute',top:50,left:20,fontSize:40, color:'#3e07bb'}}>Défis</Text>
               <Image style={{ position: 'absolute',top:22,left:'65%',zIndex:10000,width:120, height:120}} source={require('../Login/musician.png')}/>
            </View>
            <View style={{height:"80%"}}>
            <LinearGradient colors={['#801B89', '#4151AF']} style={{flex:1, padding:20}}>
            <Container>
            <Content padder>
              <Card transparent>
                <CardItem >
                  <Body style={{flex:1, flexDirection:'row'}}>
                    <View style={{width:'30%'}}><Image  style={{width:60, height:60}} source={{uri:defis[1]}} /></View>
                    <View style={{width:'70%'}}>
                    <Text style={{fontSize:20, fontWeight:'bold'}}>{defis[2]}</Text>
                      <Text>{defis[3]}</Text>
                      <Text>{defis[0]}</Text>
                    </View>
                  </Body>
                </CardItem>
                <Button onPress={() => {this.props.navigation.navigate("CoverLandAddCover")}} style={{backgroundColor:'#3498db'}} block>
                    <Text>Accéder au défi</Text>
                </Button>
              </Card>
              <Card transparent>
                <CardItem >
                  <Body style={{flex:1, flexDirection:'row'}}>
                    <View style={{width:'30%'}}><Image  style={{width:60, height:60}} source={{uri:defis[6]}} /></View>
                    <View style={{width:'70%'}}>
                    <Text style={{fontSize:20, fontWeight:'bold'}}>{defis[7]}</Text>
                      <Text>{defis[8]}</Text>
                      <Text>{defis[5]}</Text>
                    </View>
                  </Body>
                </CardItem>
                <Button onPress={() => {this.props.navigation.navigate("CoverLandAddCover")}} style={{backgroundColor:'#3498db'}} block>
                    <Text>Accéder au défi</Text>
                </Button>
              </Card>
              <Card transparent>
                <CardItem >
                  <Body style={{flex:1, flexDirection:'row'}}>
                    <View style={{width:'30%'}}><Image  style={{width:60, height:60}} source={{uri:defis[11]}} /></View>
                    <View style={{width:'70%'}}>
                    <Text style={{fontSize:20, fontWeight:'bold'}}>{defis[12]}</Text>
                      <Text>{defis[13]}</Text>
                      <Text>{defis[10]}</Text>
                    </View>
                  </Body>
                </CardItem>
                <Button onPress={() => {this.props.navigation.navigate("CoverLandAddCover")}} style={{backgroundColor:'#3498db'}} block>
                    <Text>Accéder au défi</Text>
                </Button>
              </Card>
            </Content>
          </Container>
          </LinearGradient>
          </View>
        </View>
        );
    }

    // takePicture = async function () {
    //     if (this.camera) {
    //         const options = { quality: 0.5, base64: true };
    //         const data = await this.camera.takePictureAsync(options)
    //         console.log(data.uri);
    //     }
    // };
}

export default DefiPage;

let ScreenHeight = Dimensions.get("window").height;
let ScreenWidth = Dimensions.get("window").width;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column'
    },
    preview: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center'
    },
    capture: {
        flex: 0,
        backgroundColor: '#fff',
        borderRadius: 5,
        padding: 15,
        paddingHorizontal: 20,
        alignSelf: 'center',
        margin: 20
    },
    display:{
        display:'flex'
    },
    displayNone:{
        display:'none'
    },
    check: {
        flex:1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    blank:{
        backgroundColor: '#fff'
    }
});
