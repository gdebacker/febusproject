import * as React from "react";
import {
    Container,
    Header,
    Title,
    Content,
    Text,
    Button,
    Icon,
    Left,
    Body,
    Card,
    CardItem,
    Right,
    List,
    ListItem,
    Label,
    Item,
    Input
} from "native-base";
import { Col, Row, Grid } from "react-native-easy-grid";
import LinearGradient from 'react-native-linear-gradient'
import { RNCamera } from 'react-native-camera';
//import Sound from 'react-native-sound';
//import styles from "./styles";
import { Dimensions, View, StyleSheet, TouchableOpacity, Image } from "react-native";
import SwitchButton from "../../../theme/components/SwitchButton";
//import Voice from 'react-native-voice';
//import NewCover from './NewCover';
export interface Props {
    navigation: any;
    list: any;
}
export interface State { }

class FaceRecognition extends React.Component<Props, State> {
    sound: any;
    constructor(props) {
        super(props);
        this.state = {
            finished: false
        }
    }


    componentDidMount = () =>{
        setTimeout(() => {this.setState({finished: true})}, 2000)
    }

    render() {
        const finished = this.state.finished;
        return (
            <View style={styles.container}>
                    <RNCamera
                    ref={ref => {
                        this.camera = ref;
                    }}
                    style={[styles.preview, finished ? styles.displayNone : styles.display]}
                    type={RNCamera.Constants.Type.front}
                    flashMode={RNCamera.Constants.FlashMode.off}
                    permissionDialogTitle={'Permission to use camera'}
                    permissionDialogMessage={'We need your permission to use your camera phone'}/>
            <LinearGradient colors={['#801B89', '#4151AF']} style={[styles.check,finished ? styles.display : styles.displayNone]}>
            <View >
                <Text style={{textAlign:'center'}}>
                    <Image  style={{width: 300, height: 300}} source={require('./flat_mark.png')}/>
                </Text>
                <Text style={{marginTop:30, marginBottom:30, textAlign:'center',fontSize:18, color:"#fff"}}>Reconnaissance terminée</Text>
                <Button
                        style={{backgroundColor:'#801B89', width:ScreenWidth - 60, alignSelf:'center', marginTop:30}}
                        block
                        onPress={() => this.props.navigation.navigate("ChooseHowStartYourCareer")}>
                        <Text>Continuer</Text>
                </Button>
            </View>
            </LinearGradient>
            </View>
        );
    }
    // takePicture = async function () {
    //     if (this.camera) {
    //         const options = { quality: 0.5, base64: true };
    //         const data = await this.camera.takePictureAsync(options)
    //         console.log(data.uri);
    //     }
    // };
}

export default FaceRecognition;

let ScreenHeight = Dimensions.get("window").height;
let ScreenWidth = Dimensions.get("window").width;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column'
    },
    preview: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center'
    },
    capture: {
        flex: 0,
        backgroundColor: '#fff',
        borderRadius: 5,
        padding: 15,
        paddingHorizontal: 20,
        alignSelf: 'center',
        margin: 20
    },
    display:{
        display:'flex'
    },
    displayNone:{
        display:'none'
    },
    check: {
        flex:1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    blank:{
        backgroundColor: '#fff'
    }
});
