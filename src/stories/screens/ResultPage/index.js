import * as React from "react";
import {
    Container,
    Header,
    Title,
    Content,
    Text,
    Button,
    Icon,
    Left,
    Segment,
    Body,
    Card,
    CardItem,
    Right,
    List,
    ListItem,
    Label,
    Item,
    Input
} from "native-base";
import { Col, Row, Grid } from "react-native-easy-grid";
import LinearGradient from 'react-native-linear-gradient'
import { RNCamera } from 'react-native-camera';
//import Sound from 'react-native-sound';
//import styles from "./styles";
import { Dimensions, View, StyleSheet, TouchableOpacity, Image, PanResponder } from "react-native";
import SwitchButton from "../../../theme/components/SwitchButton";
//import Voice from 'react-native-voice';
//import NewCover from './NewCover';
export interface Props {
    navigation: any;
    list: any;
}
export interface State { }
const { width, height } = Dimensions.get("window");
class ResultPage extends React.Component<Props, State> {
    sound: any;
    constructor(props) {
        super(props);
        this.state = {
            finished: false,
            defis: {},
            activeTab: "result"
        }
    }
    componentWillMount() {


    }
    componentDidMount = () =>{
        setTimeout(() => {this.setState({finished: true})}, 2000)
    }

    onTabPress(tab) {
        this.setState({ activeTab: tab });
    }

    render() {
        const finished = this.state.finished;
        const defis = this.state.defis;
        const activetab = this.state.activeTab;

        return (
            <View style={styles.container}>

                    <View style={{height:"20%", backgroundColor:'#801B89'}}>
                       <Text style={{position:'absolute',top:50,left:20,fontSize:40, color:'white'}}>Défis</Text>
                       <Image style={{ position: 'absolute',top:0,left:'65%',zIndex:10000,width:120, height:120}} source={require('../Login/musician.png')}/>
                    </View>
                    <View style={{height:"80%"}}>
                    <LinearGradient colors={['white', 'white']} style={{flex:1, padding:20}}>
                        <Segment style={{width:ScreenWidth + 20, marginLeft:-30, marginTop:-20}}>
                            <Button active={activetab == 'result'} style={{width:'50%', height: 45}} onPress={() => this.onTabPress('result')} first><Text>Résultats</Text></Button>
                            <Button active={activetab == 'other'} style={{width:'50%', height: 45}} onPress={() => this.onTabPress('other')} last><Text>Cubs</Text></Button>
                        </Segment>
                        <View style={[activetab === "result" ? styles.display : styles.displayNone,{marginTop:30}]}>
                            <Text style={{fontSize:40, alignSelf:'center', textAlign: 'center'}}><Text style={{backgroundColor:'green',color:'white', fontSize:40}}>Okay</Text>Voici<Text style={{backgroundColor:'green',color:'white', fontSize:40}}>Mon</Text> ___ en direct du hackaton</Text>
                            <View style={{backgroundColor:'gray',flex:1}}>
                            <Text>fdfdd</Text>
                            </View>
                        </View>
                        <View style={activetab === "other" ? styles.display : styles.displayNone}>
                            <Text>Test</Text>
                        </View>
                  </LinearGradient>
                  </View>
        </View>
        );
    }

    // takePicture = async function () {
    //     if (this.camera) {
    //         const options = { quality: 0.5, base64: true };
    //         const data = await this.camera.takePictureAsync(options)
    //         console.log(data.uri);
    //     }
    // };
}

export default ResultPage;

let ScreenHeight = Dimensions.get("window").height;
let ScreenWidth = Dimensions.get("window").width;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column'
    },
    preview: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center'
    },
    capture: {
        flex: 0,
        backgroundColor: '#fff',
        borderRadius: 5,
        padding: 15,
        paddingHorizontal: 20,
        alignSelf: 'center',
        margin: 20
    },
    display:{
        display:'flex'
    },
    displayNone:{
        display:'none'
    },
    check: {
        flex:1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    blank:{
        backgroundColor: '#fff'
    }
});
