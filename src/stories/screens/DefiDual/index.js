import * as React from "react";
import {
    Container,
    Header,
    Title,
    Content,
    Text,
    Button,
    Icon,
    Left,
    Body,
    Card,
    CardItem,
    Right,
    List,
    ListItem,
    Label,
    Item,
    Input
} from "native-base";
import { Col, Row, Grid } from "react-native-easy-grid";
import LinearGradient from 'react-native-linear-gradient'
import { RNCamera } from 'react-native-camera';
//import Sound from 'react-native-sound';
//import styles from "./styles";
import Video from 'react-native-video';
import { Dimensions, View, StyleSheet, TouchableOpacity, Image } from "react-native";
import SwitchButton from "../../../theme/components/SwitchButton";
//import Voice from 'react-native-voice';
//import NewCover from './NewCover';
export interface Props {
    navigation: any;
    list: any;
}
export interface State { }

class DefiDual extends React.Component<Props, State> {
    sound: any;
    constructor(props) {
        super(props);
        this.state = {
            finished: false
        }
    }


    componentDidMount = () => {
        // setTimeout(() => {this.setState({finished: true})}, 2000)
    }

    render() {
        return (
            <View style={{ flex: 1 }}>

                <View style={componentStyle1.container}>

                    <View style={componentStyle1.container1}>
                        <Video source={{ uri: 'http://test.gauthierdebacker.net/public/compo.mp4' }}   // Can be a URL or a local file.

                            ref={(ref) => {
                                this.player = ref
                            }}                                      // Store reference
                            rate={1.0}                              // 0 is paused, 1 is normal.
                            volume={1.0}                            // 0 is muted, 1 is normal.
                            muted={false}                           // Mutes the audio entirely.
                            paused={false}                          // Pauses playback entirely.
                            resizeMode="cover"                      // Fill the whole screen at aspect ratio.*
                            repeat={true}                           // Repeat forever.
                            playInBackground={false}                // Audio continues to play when app entering background.
                            playWhenInactive={false}                // [iOS] Video continues to play when control or notification center are shown.
                            ignoreSilentSwitch={"ignore"}           // [iOS] ignore | obey - When 'ignore', audio will still play with the iOS hard silent switch set to silent. When 'obey', audio will toggle with the switch. When not specified, will inherit audio settings as usual.
                            progressUpdateInterval={250.0}          // [iOS] Interval to fire onProgress (default to ~250ms)
                            onBuffer={this.onBuffer}                // Callback when remote video is buffering
                            onEnd={this.onEnd}                      // Callback when playback finishes
                            onError={this.videoError}               // Callback when video cannot be loaded
                            onFullscreenPlayerWillPresent={this.fullScreenPlayerWillPresent} // Callback before fullscreen starts
                            onFullscreenPlayerDidPresent={this.fullScreenPlayerDidPresent}   // Callback after fullscreen started
                            onFullscreenPlayerWillDismiss={this.fullScreenPlayerWillDismiss} // Callback before fullscreen stops
                            onFullscreenPlayerDidDismiss={this.fullScreenPlayerDidDissmiss}  // Callback after fullscreen stopped
                            onLoadStart={this.loadStart}            // Callback when video starts to load
                            onLoad={this.setDuration}               // Callback when video loads
                            onProgress={this.setTime}               // Callback every ~250ms with currentTime
                            onTimedMetadata={this.onTimedMetadata}  // Callback when the stream receive some metadata
                        />


                    </View>
                </View>
            </View>);
    }

    // takePicture = async function () {
    //     if (this.camera) {
    //         const options = { quality: 0.5, base64: true };
    //         const data = await this.camera.takePictureAsync(options)
    //         console.log(data.uri);
    //     }
    // };
}

export default DefiDual;


const componentStyle1 = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column'
    },
    container1: {
        flex: 1,
        flexDirection: 'row'
    },
    container2: {
        flex: 1,
        flexDirection: 'row',
        backgroundColor: 'green',
    },
})


const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column'
    },
    preview: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center'
    },
    capture: {
        flex: 0,
        borderRadius: 5,
        padding: 15,
        paddingHorizontal: 20,
        alignSelf: 'center',
        margin: 20
    },
    display: {
        display: 'flex'
    },
    displayNone: {
        display: 'none'
    },
    check: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    }
});