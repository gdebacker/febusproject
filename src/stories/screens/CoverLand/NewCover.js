import * as React from "react";
import {
    Container,
    Header,
    Title,
    Content,
    Text,
    Button,
    Icon,
    Left,
    Body,
    Right,
    List,
    ListItem,
} from "native-base";
import { Col, Row, Grid } from "react-native-easy-grid";
import LinearGradient from 'react-native-linear-gradient'
//import styles from "./styles";
import {Dimensions, View, StyleSheet, AsyncStorage, TouchableOpacity} from "react-native";
import Video from 'react-native-video';
export interface Props {
    navigation: any;
    list: any;
}
export interface State {}

class NewCover extends React.Component<Props, State> {

    constructor(props){
        super(props);

    }

    componentWillMount() {

    }

    postVideo(){








        let options = {
            headers: {
                'Authorization': "Basic YWRtaW46QGRtaW5SM2YxbmY=",
                'x-client-id': '2',
                'x-pass': '9pbLfyk0pyFy',
                'Content-Type':'multipart/form-data'
            },
            method: 'POST'
        };

        options.body = new FormData();


            options.body.append('userid', 1);
            options.body.append('vidz',{
                uri: this.props.recordedVideo,
                type : 'video/mp4',
                name: this.props.recordedVideo
            })

            fetch('http://test.gauthierdebacker.net/public/uploadVidz', options)
            .then(response => {
                return response.json()
                    .then(responseJson => {
                        console.log(responseJson);
                        return responseJson;
                    });
            });
    }

    render() {
        const recordedVideo = this.props.recordedVideo;
        return (
            <View style={{flex:1}}>

                <View style={componentStyle1.container}>

                    <View style={componentStyle1.container1}>
                        <LinearGradient colors={['#801B89', '#4151AF']} style={{flex:1, alignItems:'center' }}>
                            <Video source={{uri: recordedVideo}}   // Can be a URL or a local file.

                                   ref={(ref) => {
                                       this.player = ref
                                   }}                                      // Store reference
                                   rate={1.0}                              // 0 is paused, 1 is normal.
                                   volume={1.0}                            // 0 is muted, 1 is normal.
                                   muted={false}                           // Mutes the audio entirely.
                                   paused={false}                          // Pauses playback entirely.
                                   resizeMode="cover"                      // Fill the whole screen at aspect ratio.*
                                   repeat={true}                           // Repeat forever.
                                   playInBackground={false}                // Audio continues to play when app entering background.
                                   playWhenInactive={false}                // [iOS] Video continues to play when control or notification center are shown.
                                   ignoreSilentSwitch={"ignore"}           // [iOS] ignore | obey - When 'ignore', audio will still play with the iOS hard silent switch set to silent. When 'obey', audio will toggle with the switch. When not specified, will inherit audio settings as usual.
                                   progressUpdateInterval={250.0}          // [iOS] Interval to fire onProgress (default to ~250ms)
                                   onBuffer={this.onBuffer}                // Callback when remote video is buffering
                                   onEnd={this.onEnd}                      // Callback when playback finishes
                                   onError={this.videoError}               // Callback when video cannot be loaded
                                   onFullscreenPlayerWillPresent={this.fullScreenPlayerWillPresent} // Callback before fullscreen starts
                                   onFullscreenPlayerDidPresent={this.fullScreenPlayerDidPresent}   // Callback after fullscreen started
                                   onFullscreenPlayerWillDismiss={this.fullScreenPlayerWillDismiss} // Callback before fullscreen stops
                                   onFullscreenPlayerDidDismiss={this.fullScreenPlayerDidDissmiss}  // Callback after fullscreen stopped
                                   onLoadStart={this.loadStart}            // Callback when video starts to load
                                   onLoad={this.setDuration}               // Callback when video loads
                                   onProgress={this.setTime}               // Callback every ~250ms with currentTime
                                   onTimedMetadata={this.onTimedMetadata}  // Callback when the stream receive some metadata
                                   style={styles.backgroundVideo} />
                            <View style={{flex: 0, flexDirection: 'row', justifyContent: 'center', backgroundColor:'transparent'}}>

                                <TouchableOpacity
                                    onPress={this.postVideo.bind(this)}
                                    style = {styles.capture}
                                >
                                    <Text style={{fontSize: 14}}>Poste ta video</Text>
                                </TouchableOpacity>
                            </View>

                        </LinearGradient>
                    </View>



                </View>

            </View>
        );
    }
}

export default NewCover;

let ScreenHeight = Dimensions.get("window").height;
let ScreenWidth = Dimensions.get("window").width;

const componentStyle1 = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column'
    },
    container1: {
        flex: 1,
        flexDirection: 'row',
        backgroundColor: 'blue'
    },
    container2: {
        flex: 1,
        flexDirection: 'row',
        backgroundColor: 'green',
    },
})

const styles = StyleSheet.create({
    image: {
        alignSelf: 'center',
        height: 60,
        width: 60
    },
    fullHeight: {
        height: ScreenHeight-25,
        padding:10
    },
    textColor:{
        color:'white'
    },
    label:{
        color:'white',
        padding:10,
    },
    labelHalf:{
        color:'white',
        padding:10,
        flex:0.5,
        alignSelf:'flex-start'
    },
    inputHalf:{
        borderRadius: 4,
        borderWidth: 0.5,
        borderColor: '#d6d7da',
        backgroundColor:'rgba(255,255,255,0.3)',
        height:40,
        marginRight:5
    },
    input:{
        borderRadius: 4,
        borderWidth: 0.5,
        borderColor: '#d6d7da',
        backgroundColor:'rgba(255,255,255,0.3)',
        height:40
    },
    rowStyle:{
        marginTop: -25,
        padding:0
    },
    cardSelected:{
        backgroundColor:'rgba(255,255,255,1)'
    },
    cardNotSelected:{
        backgroundColor:'rgba(255,255,255,0.8)'
    },
    backgroundVideo: {
        position: 'absolute',
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
    },
});
