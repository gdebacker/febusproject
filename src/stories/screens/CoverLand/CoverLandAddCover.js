import * as React from "react";
import {
    Container,
    Header,
    Title,
    Content,
    Text,
    Button,
    Icon,
    Left,
    Body,
    Card,
    CardItem,
    Right,
    List,
    ListItem,
    Label,
    Item,
    Input
} from "native-base";
import { Col, Row, Grid } from "react-native-easy-grid";
import LinearGradient from 'react-native-linear-gradient'
import { RNCamera } from 'react-native-camera';
import Video from 'react-native-video';
import Sound from 'react-native-sound';
//import styles from "./styles";
import {Dimensions, View, StyleSheet,TouchableOpacity, Image} from "react-native";
import SwitchButton from "../../../theme/components/SwitchButton";
import Voice from 'react-native-voice';
import ResultPage from '../ResultPage';
export interface Props {
    navigation: any;
    list: any;
}
export interface State {}

class CoverLandAddCover extends React.Component<Props, State> {
    sound: any;
    proba:any;
    constructor(props){
        super(props);
        this.proba=[0,0,0,0,0,0,0,0,0,0,1,1,1,1,2,2];
        this.state = {
          soundLink : "",
          playingSound:"no",
            cameraOpen:false,
            recording:false,
            recordedVideo:null,
            voiceJustness: 0,
            displayPastille:false,
            voiceText: '',
            defi:[]
        }
        Voice.onSpeechStart = this.onSpeechStart.bind(this);
        Voice.onSpeechRecognized = this.onSpeechRecognized.bind(this);
        Voice.onSpeechEnd = this.onSpeechEnd.bind(this);
        Voice.onSpeechError = this.onSpeechError.bind(this);
        Voice.onSpeechResults = this.onSpeechResults.bind(this);
        Voice.onSpeechPartialResults = this.onSpeechPartialResults.bind(this);
        Voice.onSpeechVolumeChanged = this.onSpeechVolumeChanged.bind(this);
    }
    componentWillMount() {

        let options = {
            headers: {
                'Authorization': "Basic YWRtaW46QGRtaW5SM2YxbmY=",
                'x-client-id': '2',
                'x-pass': '9pbLfyk0pyFy',
            },
            method: 'GET'
        };


        fetch('http://test.gauthierdebacker.net/public/getDefi4User', options)
            .then(response => {
                return response.json()
                    .then(responseJson => {
                        //console.log("defiiiiiiiiiiiiiiiiiiiiii3333333333");
                        //console.log(responseJson);
                        //console.log(responseJson.user);
                        this.setState({
                            defi: responseJson.user
                        })
                        return responseJson;
                    });
            });

        this.sound = new Sound("empty", null, (error) => {
            if (error) {
                console.log(error);
                return;
            }
        });
    }

    componentWillUnmount = () =>{
            this.sound.reset()
        Voice.destroy().then(Voice.removeAllListeners);
    }



    onSpeechStart(e) {

    }

    onSpeechRecognized(e) {

    }

    onSpeechEnd(e) {

    }

    onSpeechError(e) {
        console.log(e);
    }

    onSpeechResults(e) {

    }

    onSpeechPartialResults(e) {
        //console.log(e);
        //console.log('voiceJustness : ' + this.state.voiceJustness);
        this.setState({
            voiceText: e.value
        })

    }

    onSpeechVolumeChanged(e) {

    }



    start = () => {

        this.setState({
            cameraOpen:true
        });

       /* this.sound = new Sound("track.mp3", null, (error) => {
            if (error) {
                console.log(error);
                return;
            }

            this.state.playingSound = "yes";
            // play when loaded
            this.sound.play();
        });*/


    }


    recordVideo = async () => {
        //console.log(this.player);
        //this.player.props.volume = 0;
        console.log("we will start recording");
        console.log(this.state.recording);
        if (this.camera) {
            const options = { quality: 0.5, base64: true };
            if(!this.state.recording){
                //Voice.start('en-US');
                this.playSound();
                setTimeout(() => {
                    setInterval(() => {
                        this.setState({
                            displayPastille: true,
                            voiceJustness: this.proba[Math.floor(Math.random() * this.proba.length)]
                        });
                    }, 1300);
                }, 500);
                this.setState({
                    recording:true
                })
                console.log("recording...");
                const data = await this.camera.recordAsync({
                    quality: RNCamera.Constants.VideoQuality['480p'],
                    mute:false
                });
                console.log(data);
                console.log(data.uri);
                this.setState({
                    recordedVideo:data.uri
                });
                this.sound.reset();
            }
            else{
                //Voice.stop();
                this.camera.stopRecording();
                this.setState({
                    recording:false
                })
            }

        }
    };

    playSound = () => {
        console.log("toto");
        if(this.state.playingSound == "yes"){
            this.sound.reset();
            this.playingSound = "no";
        }
        else{
            this.sound = new Sound("batbat.mp3", null, (error) => {
                if (error) {
                    console.log(error);
                    return;
                }

                this.state.playingSound = "yes";
                // play when loaded
                this.sound.play();
            });
        }


    }

    render() {
        const cameraOpen = this.state.cameraOpen;
        const recordedVideo = this.state.recordedVideo;
        const displayPastille = this.state.displayPastille;
        const voiceJustness = this.state.voiceJustness;
        const voiceText = this.state.voiceText;
        const defi = this.state.defi;
        const recording = this.state.recording;
        return (
            <View style={{flex:1}}>


                <View style={componentStyle1.container}>
                    <Header style={{backgroundColor:'#801B89'}}>


                        <Left>
                            <Button transparent onPress={() => { this.props.navigation.goBack()}}>
                                <Icon name="ios-arrow-back" />
                            </Button>
                        </Left>


                    </Header>

                    <View style={[styles.container, recordedVideo != null ? styles.display : styles.displayNone]}>
                        <ResultPage recordedVideo={recordedVideo}></ResultPage>
                    </View>

                    <View style={[styles.container, cameraOpen && recordedVideo == null ? styles.display : styles.displayNone]}>
                        <RNCamera
                            ref={ref => {
                                this.camera = ref;
                            }}
                            style = {{ flex: 1,
                                justifyContent: 'flex-end',
                                alignItems: 'center'}}
                            type={RNCamera.Constants.Type.front}
                            flashMode={RNCamera.Constants.FlashMode.off}
                            permissionDialogTitle={'Permission to use camera'}
                            permissionDialogMessage={'We need your permission to use your camera phone'}
                        />
                        <View style={{flex: 0, flexDirection: 'row', justifyContent: 'center', backgroundColor:'transparent'}}>
                            <Text style={{color:'white'}}>{voiceText}</Text>
                            <TouchableOpacity
                                onPress={this.recordVideo.bind(this)}
                                style = {styles.capture}
                            >
                                <Text style={{fontSize: 14}}> {this.state.recording ? "Stoppe l'enregistrement...": "Chante sur cet instument !"} </Text>
                            </TouchableOpacity>
                        </View>
                        <View style={{position:'absolute',left:135,bottom:75}}>
                            <View style={[
                                {marginLeft:20, width:50, height:50, borderRadius:50},
                                displayPastille ? styles.display : styles.displayNone,
                                voiceJustness == 0 ? styles.green : voiceJustness == 1 ? styles.yellow : styles.red
                                ]}><Text> </Text></View>

                        </View>
                    </View>

                    <View style={[componentStyle1.container1, cameraOpen || recordedVideo ? styles.displayNone : styles.display]}>
                        <LinearGradient colors={['#801B89', '#4151AF']} style={{flex:1}}>
                            <View style={{flex:1, flexDirection:'row'}}>
                            <Video source={{uri: defi[5]}}   // Can be a URL or a local file.

                                   ref={(ref) => {
                                       this.player = ref
                                   }}                                      // Store reference
                                   rate={1.0}                              // 0 is paused, 1 is normal.
                                   volume={1.0}                            // 0 is muted, 1 is normal.
                                   muted={recording ? true : false}                           // Mutes the audio entirely.
                                   paused={false}                          // Pauses playback entirely.
                                   resizeMode="cover"                      // Fill the whole screen at aspect ratio.*
                                   repeat={true}                           // Repeat forever.
                                   playInBackground={false}                // Audio continues to play when app entering background.
                                   playWhenInactive={false}                // [iOS] Video continues to play when control or notification center are shown.
                                   ignoreSilentSwitch={"ignore"}           // [iOS] ignore | obey - When 'ignore', audio will still play with the iOS hard silent switch set to silent. When 'obey', audio will toggle with the switch. When not specified, will inherit audio settings as usual.
                                   progressUpdateInterval={250.0}          // [iOS] Interval to fire onProgress (default to ~250ms)
                                   onBuffer={this.onBuffer}                // Callback when remote video is buffering
                                   onEnd={this.onEnd}                      // Callback when playback finishes
                                   onError={this.videoError}               // Callback when video cannot be loaded
                                   onFullscreenPlayerWillPresent={this.fullScreenPlayerWillPresent} // Callback before fullscreen starts
                                   onFullscreenPlayerDidPresent={this.fullScreenPlayerDidPresent}   // Callback after fullscreen started
                                   onFullscreenPlayerWillDismiss={this.fullScreenPlayerWillDismiss} // Callback before fullscreen stops
                                   onFullscreenPlayerDidDismiss={this.fullScreenPlayerDidDissmiss}  // Callback after fullscreen stopped
                                   onLoadStart={this.loadStart}            // Callback when video starts to load
                                   onLoad={this.setDuration}               // Callback when video loads
                                   onProgress={this.setTime}               // Callback every ~250ms with currentTime
                                   onTimedMetadata={this.onTimedMetadata}  // Callback when the stream receive some metadata
                                   style={[styles.backgroundVideo,{flex:0.8}]} />
                                <View style={{flex: 1, justifyContent: 'center', backgroundColor:'transparent'}}>
                                    <Text style={{color:'white'}}>{voiceText}</Text>
                                    <TouchableOpacity
                                        onPress={this.start}
                                        style = {styles.capture}
                                    >
                                        <Text style={{fontSize: 14}}>Répond à ce défi</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </LinearGradient>
                    </View>



                </View>

            </View>

        );
    }
}

export default CoverLandAddCover;

let ScreenHeight = Dimensions.get("window").height;
let ScreenWidth = Dimensions.get("window").width;

const componentStyle1 = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column'
    },
    container1: {
        flex: 1,
        flexDirection: 'row',
        backgroundColor: 'blue'
    },
    container2: {
        flex: 1,
        flexDirection: 'row',
        backgroundColor: 'green',
    },
})

const styles = StyleSheet.create({
    capture: {
        flex: 0,
        backgroundColor: '#fff',
        borderRadius: 5,
        padding: 15,
        paddingHorizontal: 20,
        alignSelf: 'center',
        margin: 20
    },
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: 'black'
    },
    image: {
        alignSelf: 'center',
        height: 60,
        width: 60
    },
    fullHeight: {
        height: ScreenHeight-25,
        padding:10
    },
    textColor:{
        color:'white'
    },
    label:{
        color:'white',
        padding:10,
    },
    labelHalf:{
        color:'white',
        padding:10,
        flex:0.5,
        alignSelf:'flex-start'
    },
    inputHalf:{
        borderRadius: 4,
        borderWidth: 0.5,
        borderColor: '#d6d7da',
        backgroundColor:'rgba(255,255,255,0.3)',
        height:40,
        marginRight:5
    },
    input:{
        borderRadius: 4,
        borderWidth: 0.5,
        borderColor: '#d6d7da',
        backgroundColor:'rgba(255,255,255,0.3)',
        height:40
    },
    rowStyle:{
        marginTop: -25,
        padding:0
    },
    cardSelected:{
        backgroundColor:'rgba(255,255,255,1)'
    },
    cardNotSelected:{
        backgroundColor:'rgba(255,255,255,0.8)'
    },
    display:{
        display:'flex'
    },
    displayNone:{
        display:'none'
    },
    green:{
        backgroundColor:'green'
    },
    yellow:{
        backgroundColor:'yellow'
    },
    red:{
        backgroundColor:'red'
    },
    backgroundVideo: {
        position: 'absolute',
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
    }
});
