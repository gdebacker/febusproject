import * as React from "react";
import { Image, Platform, Picker, StyleSheet, Dimensions, TouchableOpacity} from "react-native";
import { Container, Content, Card, CardItem, Header, Body, Title, Button, Form, Input, Item, Label, Text, View, Icon, Footer } from "native-base";
import { Col, Row, Grid } from "react-native-easy-grid";
//import styles from "./styles";
import styled from 'styled-components';
import LinearGradient from 'react-native-linear-gradient';
import * as Progress from 'react-native-progress';
export interface Props {
	loginForm: any;
	onChangeStep: Function;
	data:any;
	accountObjectState:any;
}
export interface State {
	userObject : any;
	artistic_domain:any;
	artistic_style:any;
}
class Login extends React.Component<Props, State> {
    userObject:any;
    state
	constructor(props){
		super(props);
        this.userObject = {
            username : "",
            lastname: "",
            firstname:"",
            birth:"",
            phone:"",
            gender:"",
            domain: "",
            style: "",
            step: 1
        };
        this.state = {
            username:"",
            artistic_domain: "",
            artistic_style: "",
            genderSelected: ""
        }
		if(this.props.accountObjectState != null){
            this.userObject = this.props.accountObjectState;
            this.userObject.step = 1;
            this.state.genderSelected = this.props.accountObjectState.gender;
        }
	}
	render() {
		const artistic_domains = this.props.data.artistic_domains ? this.props.data.artistic_domains : [];
        const artistic_styles = this.props.data.artistic_styles ? this.props.data.artistic_styles : [];
        const artistic_domains_picker_items = [];
        artistic_domains.forEach(function(domain,key) {
            artistic_domains_picker_items.push(
                <Picker.Item key={key} label={domain.displayName} value={domain.name} />
            )
        });
        const artistic_styles_picker_items = [];
        artistic_styles.forEach(function(style,key) {
            artistic_styles_picker_items.push(
                <Picker.Item key={key} label={style.displayName} value={style.name} />
            )
        });
		return (
		    <Container>

                <Header style={{backgroundColor:'#801B89'}}>


                    <Body>
                    <Title>Informations générales (1/3)</Title>
                    </Body>


                </Header>

		    <Content>
            <Grid>
                <Row size={100}>
                    <Col style={{backgroundColor:'white'}}>
                        <Progress.Bar progress={0.33} width={null} height={20} color={'orange'} style={{borderRadius:0,borderWidth:0, marginLeft:-1}}/>
                        <LinearGradient colors={['#801B89', '#4151AF']} style={styles.fullHeight}>

                                    <Row>
                                        <Col style={{marginTop:0}}>
                                            <Label style={styles.label}>Pseudo</Label>
                                            <Item>
                                                <Input style={styles.input} defaultValue={this.userObject.username} onChangeText={ (text) => this.userObject.username = text } />
                                            </Item>
                                        </Col>
                                    </Row>
                                    <Row style={styles.rowStyle}>
                                        <Col style={{marginTop:-15}}>
                                            <Label style={styles.label}>Nom</Label>
                                            <Item>
                                                <Input style={styles.inputHalf} defaultValue={this.userObject.lastname} onChangeText={ (text) => this.userObject.lastname = text }/>
                                            </Item>
                                        </Col>
                                        <Col style={{marginTop:-15}}>
                                            <Label style={styles.label}>Prénom</Label>
                                            <Item>
                                                <Input style={styles.input} defaultValue={this.userObject.firstname} onChangeText={ (text) => this.userObject.firstname = text }/>
                                            </Item>
                                        </Col>
                                    </Row>
                                    <Row style={styles.rowStyle}>
                                        <Col style={{marginTop:-30}}>
                                            <Label style={styles.label}>Naissance</Label>
                                            <Item>
                                                <Input style={styles.inputHalf} defaultValue={this.userObject.birth} onChangeText={ (text) => this.userObject.birth = text }/>
                                            </Item>
                                        </Col>
                                        <Col style={{marginTop:-30}}>
                                            <Label style={styles.label}>Téléphone</Label>
                                            <Item>
                                                <Input style={styles.input} defaultValue={this.userObject.phone} onChangeText={ (text) => this.userObject.phone = text }/>
                                            </Item>
                                        </Col>
                                    </Row>

                                        <Row style={styles.rowStyle}>
                                            <Col style={{marginTop:-15, height:150}}>
                                                <Label style={{color:'white', padding:10, marginTop:-30}}>Genre</Label>

                                                    <Card style={ this.state.genderSelected == "woman" ? styles.cardSelected : styles.cardNotSelected}>
                                                        <TouchableOpacity onPress={() => {this.userObject.gender = "woman"; this.setState({genderSelected:"woman"})}}>
                                                        <CardItem style={{backgroundColor:'transparent'}}>
                                                            <Body>
                                                            <Image
                                                                style={styles.image}
                                                                source={require('./woman.png')}
                                                                resizeMode="stretch"
                                                            />
                                                            </Body>
                                                        </CardItem>
                                                        </TouchableOpacity>
                                                    </Card>

                                            </Col>
                                            <Col style={{marginTop:-15, height:150}}>
                                                <Label style={{color:'white', padding:10, marginTop:-30}}> </Label>
                                                <Card style={ this.state.genderSelected == "man" ? styles.cardSelected : styles.cardNotSelected} >
                                                    <TouchableOpacity onPress={() => {this.userObject.gender = "man"; this.setState({genderSelected:"man"})}}>
                                                        <CardItem style={{backgroundColor:'transparent'}}>
                                                            <Body>
                                                            <Image
                                                                style={styles.image}
                                                                source={require('./man.png')}
                                                                resizeMode="stretch"
                                                            />
                                                            </Body>
                                                        </CardItem>
                                                    </TouchableOpacity>
                                                </Card>
                                            </Col>
                                        </Row>
                                    <Row>
                                        <Col style={{marginTop:20}}>
                                            <Button style={{backgroundColor:'#801B89'}} block onPress={() =>{this.props.onChangeStep(this.userObject, "forward")}}>
                                                <Text>Suivant</Text>
                                            </Button>
                                        </Col>
                                    </Row>
                        </LinearGradient>
                    </Col>
                </Row>
			</Grid>
            </Content>
            </Container>
		);
	}
}

export default Login;

let ScreenHeight = Dimensions.get("window").height;

const styles = StyleSheet.create({
    image: {
        alignSelf: 'center',
        height: 75,
        width: 75
    },
    fullHeight: {
        height: ScreenHeight-100,
        padding:10
    },
    textColor:{
        color:'white'
    },
    label:{
        color:'white',
        padding:10,
    },
    labelHalf:{
        color:'white',
        padding:10,
        flex:0.5,
        alignSelf:'flex-start'
    },
    inputHalf:{
        borderRadius: 4,
        borderWidth: 0.5,
        borderColor: '#d6d7da',
        backgroundColor:'rgba(255,255,255,0.3)',
        height:40,
        marginRight:5
    },
    input:{
        borderRadius: 4,
        borderWidth: 0.5,
        borderColor: '#d6d7da',
        backgroundColor:'rgba(255,255,255,0.3)',
        height:40
    },
    rowStyle:{
        marginTop: -25,
        padding:0
    },
    cardSelected:{
        backgroundColor:'rgba(255,255,255,1)'
    },
    cardNotSelected:{
        backgroundColor:'rgba(255,255,255,0.8)'
    }
});
