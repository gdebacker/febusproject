import * as React from "react";
import { Image, Platform, Picker, Dimensions, StyleSheet, TouchableOpacity } from "react-native";
import { Container, Content, Header, Body, Title, Button, DeckSwiper, Thumbnail, Form, Input, Card, CardItem, Item, Label, Text, View, Icon, Footer, Left, Grid, Row, Col } from "native-base";
import LinearGradient from 'react-native-linear-gradient'
import Carousel, { Pagination } from 'react-native-snap-carousel';
import SliderEntry from '../../../theme/components/SliderEntry';
import * as Progress from 'react-native-progress';
//import styles from "./styles";
export interface Props {
    loginForm: any,
    onLogin: Function,
    data:any;
}
export interface State {
    userObject : any;
    domain:any;
    artistic_style:any;
}
class AvatarSelection extends React.Component<Props, State> {
    userObject:any;
    state
    constructor(props){
        super(props);
        this.userObject = this.props.userObject;
        this.state = {
            domain: "",
            style: "",

        }
    }

    _renderItemWithParallax ({item, index}, parallaxProps) {
        return (
            <SliderEntry
                data={item}
                even={(index + 1) % 2 === 0}
                parallax={true}
                parallaxProps={parallaxProps}
            />
        );
    }
    render() {
        const stylesArt = [
            {
                domain: ["singer","beatmaker"],
                title: "Classique",
                subtitle: "Le style de base de Mozart",
                illustration: "https://i.imgur.com/UYiroysl.jpg",
            },
            {
                domain: ["singer","beatmaker"],
                title: "Rock",
                subtitle: "Tu aimes l\"électricité ?",
                illustration: "https://i.imgur.com/UPrs1EWl.jpg"
            },
            {
                domain: ["singer","beatmaker"],
                title: "Jazz",
                subtitle: "Lorem ipsum dolor sit amet et nuncat ",
                illustration: "https://i.imgur.com/MABUbpDl.jpg"
            },
            {
                domain: ["singer","beatmaker"],
                title: "Soul",
                subtitle: "Lorem ipsum dolor sit amet et nuncat mergitur",
                illustration: "https://i.imgur.com/KZsmUi2l.jpg"
            },
            {
                domain: ["musician"],
                title: "Piano",
                subtitle: "Lorem ipsum dolor sit amet",
                illustration: "https://i.imgur.com/2nCt3Sbl.jpg"
            },
            {
                domain: ["musician"],
                title: "Batterie",
                subtitle: "Lorem ipsum dolor sit amet",
                illustration: "https://i.imgur.com/lceHsT6l.jpg"
            }
        ];

        const filteredStylesArt = stylesArt.filter(style => style.domain.includes(this.state.domain));


        return (
            <Container>

                <Header style={{backgroundColor:'#801B89'}}>
                    <Left>
                        <Button transparent onPress={() => {  this.props.onChangeStep(this.userObject, 'back')}}>
                            <Icon name="ios-arrow-back" />
                        </Button>
                    </Left>

                    <Body>
                    <Title>Choix de l'avatar (3/3)</Title>
                    </Body>


                </Header>

                <Content>
                    <Grid>
                        <Row size={100}>
                            <Col style={{backgroundColor:'white'}}>
                                <Progress.Bar progress={1} width={null} height={20} color={'orange'} style={{borderRadius:0,borderWidth:0, marginLeft:-1}}/>
                                <LinearGradient colors={['#801B89', '#4151AF']} style={styles.fullHeight}>

                                    <Row>
                                        <Col style={{marginTop:20}}>
                                            <Button style={{backgroundColor:'#801B89'}} block onPress={() =>{ this.props.onChangeStep(this.userObject, "forward")}}>
                                                <Text>Suivant</Text>
                                            </Button>
                                        </Col>
                                    </Row>

                                </LinearGradient>
                            </Col>
                        </Row>
                    </Grid>
                </Content>
            </Container>
        );
    }
}

export default AvatarSelection;

let ScreenHeight = Dimensions.get("window").height;

const styles = StyleSheet.create({
    image: {
        alignSelf: 'center',
        height: 60,
        width: 60
    },
    fullHeight: {
        height: ScreenHeight-100,
        padding:10
    },
    textColor:{
        color:'white'
    },
    label:{
        color:'white',
        padding:10,
    },
    labelHalf:{
        color:'white',
        padding:10,
        flex:0.5,
        alignSelf:'flex-start'
    },
    inputHalf:{
        borderRadius: 4,
        borderWidth: 0.5,
        borderColor: '#d6d7da',
        backgroundColor:'rgba(255,255,255,0.3)',
        height:40,
        marginRight:5
    },
    input:{
        borderRadius: 4,
        borderWidth: 0.5,
        borderColor: '#d6d7da',
        backgroundColor:'rgba(255,255,255,0.3)',
        height:40
    },
    rowStyle:{
        marginTop: -25,
        padding:0
    },
    cardSelected:{
        backgroundColor:'rgba(255,255,255,1)'
    },
    cardNotSelected:{
        backgroundColor:'rgba(255,255,255,0.8)'
    }
});


const { width: viewportWidth, height: viewportHeight } = Dimensions.get('window');

function wp (percentage) {
    const value = (percentage * viewportWidth) / 100;
    return Math.round(value);
}

const slideHeight = viewportHeight * 0.36;
const slideWidth = wp(75);
const itemHorizontalMargin = wp(2);

const sliderWidth = viewportWidth;

const itemWidth = slideWidth + itemHorizontalMargin * 2;
