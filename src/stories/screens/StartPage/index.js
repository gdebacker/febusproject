import * as React from "react";
import {
    Container,
    Header,
    Title,
    Content,
    Text,
    Button,
    Icon,
    Left,
    Body,
    Card,
    CardItem,
    Right,
    List,
    ListItem,
    Label,
    Item,
    Input
} from "native-base";
import { Col, Row, Grid } from "react-native-easy-grid";
import LinearGradient from 'react-native-linear-gradient'
import { RNCamera } from 'react-native-camera';
//import Sound from 'react-native-sound';
//import styles from "./styles";
import { Dimensions, View, StyleSheet, TouchableOpacity, Image, PanResponder,ActivityIndicator } from "react-native";
import SwitchButton from "../../../theme/components/SwitchButton";
//import Voice from 'react-native-voice';
//import NewCover from './NewCover';
export interface Props {
    navigation: any;
    list: any;
}
export interface State { }


class StartPage extends React.Component<Props, State> {
    sound: any;
    constructor(props) {
        super(props);
        this.state = {
            finished: false,
        }
    }
        
    componentDidMount = () =>{
        setTimeout(() => { this.props.navigation.navigate("FaceRecognition")}, 2000)
    }

    render() {
        const finished = this.state.finished;
    
        return (
        <View style={styles.container}>
           <Image style={{width:150, height:150, marginBottom:10}} source={require('./febus.png')}/>
           <Text style={{color: '#6b07bb', fontSize:30, fontWeight:'bold', marginBottom: 40}}>Fébus</Text>
           <ActivityIndicator size="large" color="#a50c87" />
        </View>
       );
    }

    // takePicture = async function () {
    //     if (this.camera) {
    //         const options = { quality: 0.5, base64: true };
    //         const data = await this.camera.takePictureAsync(options)
    //         console.log(data.uri);
    //     }
    // };
}

export default StartPage;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent:'center'
    },
    preview: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center'
    },
    capture: {
        flex: 0,
        backgroundColor: '#fff',
        borderRadius: 5,
        padding: 15,
        paddingHorizontal: 20,
        alignSelf: 'center',
        margin: 20
    },
    display:{
        display:'flex'
    },
    displayNone:{
        display:'none'
    },
    check: {
        flex:1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    blank:{
        backgroundColor: '#fff'
    }
});
