import * as React from "react";
import {
  Container,
  Header,
  Title,
  Content,
  Text,
  Button,
  Icon,
  Left,
  Body,
  Right,
  List,
  ListItem,
    Footer,
    Card,
    CardItem,
    ActionSheet,
    Badge

} from "native-base";
import Video from 'react-native-video';
//import styles from "./
import { Col, Row, Grid } from "react-native-easy-grid";
import LinearGradient from 'react-native-linear-gradient'
import {PanResponder, Dimensions, View, ScrollView, StyleSheet, Image} from "react-native";
export interface Props {
  navigation: any;
  list: any;
}
export interface State {}

const { width, height } = Dimensions.get("window");

const getDirectionAndColor = ({ moveX, moveY, dx, dy }) => {
    const draggedDown = dy > 30;
    const draggedUp = dy < -30;
    const draggedLeft = dx < -30;
    const draggedRight = dx > 30;
    const isRed = moveY < 90 && moveY > 40 && moveX > 0 && moveX < width;
    const isBlue = moveY > height - 50 && moveX > 0 && moveX < width;
    let dragDirection = "";

    if (draggedDown || draggedUp) {
        if (draggedDown) dragDirection += "dragged down";
        if (draggedUp) dragDirection += "dragged up";
    }

    if (draggedLeft || draggedRight) {
        if (draggedLeft) dragDirection = "dragged left";
        if (draggedRight) dragDirection = "dragged right";
    }

    if (isRed) return `red ${dragDirection}`;
    if (isBlue) return `blue ${dragDirection}`;
    if (dragDirection) return dragDirection;
};

class Home extends React.Component<Props, State> {

    interacted : false;

    constructor(props){
        super(props);
        this.state = {
            wallvideos: {}
        }
    }

    componentWillReceiveProps(){


    }

    componentWillMount() {


        let options = {
            headers: {
                'Authorization': "Basic YWRtaW46QGRtaW5SM2YxbmY=",
                'x-client-id': '2',
                'x-pass': '9pbLfyk0pyFy',
            },
            method: 'GET'
        };


        fetch('http://test.gauthierdebacker.net/public/getAllVidz', options)
            .then(response => {
                return response.json()
                    .then(responseJson => {
                        console.log(responseJson);
                        this.setState({
                            wallvideos: responseJson.users
                        })
                        return responseJson;
                    });
            });



        this._panResponder = PanResponder.create({
            onMoveShouldSetPanResponder: (evt, gestureState) => {
                if(gestureState.dy !== 0 || gestureState.dx !== 0){return true;} else{return false;}
            },
            onPanResponderMove: (evt, gestureState) => {
                // The most recent move distance is gestureState.move{X,Y}
                const drag = getDirectionAndColor(gestureState);
                if ( drag === "dragged right" && !this.interacted) {
                    this.interacted = true;
                    this.props.navigation.navigate("DefiPage", {
                        transition: 'myCustomTransitionLeft'
                    });
                    setTimeout(() => {this.interacted = false}, 1000)
                }
            },

        });
    }

    loadStart = () => {

    }

    getWallVideos = () => {

    }

  render() {
        this.getWallVideos();
      const BUTTONS = ["Enregistrer une cover", "Option 1", "Option 2", "Delete", "Cancel"];
      const DESTRUCTIVE_INDEX = 3;
      const CANCEL_INDEX = 4;
      const wallvideos = this.state.wallvideos;
      const videosToDisplay = [];
      for (var i=0; i < wallvideos.length; i++) {
          videosToDisplay.push(
            <View style={{padding:15}}>
            <View style={{transform:[{'translate':[0,0,1]}]}}>
              <Card key={i}>

                  <Video source={{uri: wallvideos[i][1] }}   // Can be a URL or a local file.

                         ref={(ref) => {
                             this.player = ref
                         }}                                      // Store reference
                         rate={1.0}                              // 0 is paused, 1 is normal.
                         volume={1.0}                            // 0 is muted, 1 is normal.
                         muted={false}                           // Mutes the audio entirely.
                         paused={false}                          // Pauses playback entirely.
                         resizeMode="cover"                      // Fill the whole screen at aspect ratio.*
                         repeat={true}                           // Repeat forever.
                         playInBackground={false}                // Audio continues to play when app entering background.
                         playWhenInactive={false}                // [iOS] Video continues to play when control or notification center are shown.
                         ignoreSilentSwitch={"ignore"}           // [iOS] ignore | obey - When 'ignore', audio will still play with the iOS hard silent switch set to silent. When 'obey', audio will toggle with the switch. When not specified, will inherit audio settings as usual.
                         progressUpdateInterval={250.0}          // [iOS] Interval to fire onProgress (default to ~250ms)
                         onBuffer={this.onBuffer}                // Callback when remote video is buffering
                         onEnd={this.onEnd}                      // Callback when playback finishes
                         onError={this.videoError}               // Callback when video cannot be loaded
                         onFullscreenPlayerWillPresent={this.fullScreenPlayerWillPresent} // Callback before fullscreen starts
                         onFullscreenPlayerDidPresent={this.fullScreenPlayerDidPresent}   // Callback after fullscreen started
                         onFullscreenPlayerWillDismiss={this.fullScreenPlayerWillDismiss} // Callback before fullscreen stops
                         onFullscreenPlayerDidDismiss={this.fullScreenPlayerDidDissmiss}  // Callback after fullscreen stopped
                         onLoadStart={() => {this.loadStart()}}            // Callback when video starts to load
                         onLoad={this.setDuration}               // Callback when video loads
                         onProgress={this.setTime}               // Callback every ~250ms with currentTime
                         onTimedMetadata={this.onTimedMetadata}  // Callback when the stream receive some metadata
                         style={styles.blurvideo} />
                  <Video source={{ uri: wallvideos[i][1]}}   // Can be a URL or a local file.

                         ref={(ref) => {
                             this.player = ref
                         }}                                      // Store reference
                         rate={1.0}                              // 0 is paused, 1 is normal.
                         volume={1.0}                            // 0 is muted, 1 is normal.
                         muted={false}                           // Mutes the audio entirely.
                         paused={false}                          // Pauses playback entirely.
                         resizeMode="contain"                      // Fill the whole screen at aspect ratio.*
                         repeat={true}                           // Repeat forever.
                         playInBackground={false}                // Audio continues to play when app entering background.
                         playWhenInactive={false}                // [iOS] Video continues to play when control or notification center are shown.
                         ignoreSilentSwitch={"ignore"}           // [iOS] ignore | obey - When 'ignore', audio will still play with the iOS hard silent switch set to silent. When 'obey', audio will toggle with the switch. When not specified, will inherit audio settings as usual.
                         progressUpdateInterval={250.0}          // [iOS] Interval to fire onProgress (default to ~250ms)
                         onBuffer={this.onBuffer}                // Callback when remote video is buffering
                         onEnd={this.onEnd}                      // Callback when playback finishes
                         onError={this.videoError}               // Callback when video cannot be loaded
                         onFullscreenPlayerWillPresent={this.fullScreenPlayerWillPresent} // Callback before fullscreen starts
                         onFullscreenPlayerDidPresent={this.fullScreenPlayerDidPresent}   // Callback after fullscreen started
                         onFullscreenPlayerWillDismiss={this.fullScreenPlayerWillDismiss} // Callback before fullscreen stops
                         onFullscreenPlayerDidDismiss={this.fullScreenPlayerDidDissmiss}  // Callback after fullscreen stopped
                         onLoadStart={this.loadStart}            // Callback when video starts to load
                         onLoad={this.setDuration}               // Callback when video loads
                         onProgress={this.setTime}               // Callback every ~250ms with currentTime
                         onTimedMetadata={this.onTimedMetadata}  // Callback when the stream receive some metadata
                         style={styles.backgroundVideo} />

              </Card>
              </View>
                  <View style={{position:'absolute', top:-7,left:5, borderRadius:90,backgroundColor:'white',
                  transform:[{'translate':[0,0,1]}]}}>
                      <Badge style={{ width:75, height:75, borderRadius:75, backgroundColor:'white'}} >
                          <Image
                              style={{width: 50, height: 50, alignSelf:'center', marginTop:10}}
                              source={{uri: wallvideos[i][2]}}
                          />
                      </Badge>
                  </View>

                <View style={{flex:1,flexDirection:'row'}}>
                    <View style={{flex:0.25}}>
                        <Button style={{height:35, backgroundColor:'orange'}}><Text style={{fontSize:12}}>CoreML</Text></Button>
                    </View>
                      <View style={{flex:0.25}}>
                          <Button style={{height:35, backgroundColor:'orange'}}><Text style={{fontSize:12}}>Flex</Text></Button>
                      </View>
                    <View style={{flex:0.50}}>
                        <Image
                            style={{width: 25, height: 25, alignSelf: 'flex-end'}}
                            source={require('./likee.png')}
                        />
                    </View>
                </View>
              </View>
          )
      }
    return (
        <View {...this._panResponder.panHandlers} style={{flex:1}}>

            <View style={componentStyle1.container}>
                <Header style={{backgroundColor:'#801B89'}}>


                    <Body style={{flex:1, flexDirection:'row'}}>
                    {/* <Icon   style={{marginTop:0,fontSize: 30, color: 'white' ,flex:0.3}}/> */}
                    <Title style={{flex:0.4}}>Wall</Title>
                    </Body>


                </Header>

                <View style={componentStyle1.container1}>
                    <LinearGradient colors={['#801B89', '#4151AF']} style={{flex:1, flexDirection:'column' }}>
                        <ScrollView style={{paddingTop:10}}>
                            {videosToDisplay}


                        </ScrollView>

                    </LinearGradient>
                    <View style={{position:'absolute',bottom:10,width:ScreenWidth, backgroundColor:'transparent'}}>
                        <View>
                        <Button
                            rounded style={{width:50, alignSelf:'center', borderWidth:10, borderColor:'orange', backgroundColor:'white'}}
                            onPress={() =>
                                ActionSheet.show(
                                    {
                                        options: BUTTONS,
                                        cancelButtonIndex: CANCEL_INDEX,
                                        destructiveButtonIndex: DESTRUCTIVE_INDEX
                                    },
                                    buttonIndex => {
                                        if(buttonIndex == 0){
                                            this.props.navigation.navigate("CoverLandAddCover");
                                        }
                                    }
                                )}
                        >

                        </Button>
                        </View>
                    </View>



                </View>



            </View>

        </View>
    );
  }
}

export default Home;


let ScreenHeight = Dimensions.get("window").height;
let ScreenWidth = Dimensions.get("window").width;

const componentStyle1 = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column'
    },
    container1: {
        flex: 1,
        flexDirection: 'row',
        backgroundColor: 'blue'
    },
    container2: {
        flex: 1,
        flexDirection: 'row',
        backgroundColor: 'green',
    },
})

const styles = StyleSheet.create({
    image: {
        alignSelf: 'center',
        height: 60,
        width: 60
    },
    fullHeight: {
        height: ScreenHeight-25,
        padding:10
    },
    textColor:{
        color:'white'
    },
    label:{
        color:'white',
        padding:10,
    },
    labelHalf:{
        color:'white',
        padding:10,
        flex:0.5,
        alignSelf:'flex-start'
    },
    inputHalf:{
        borderRadius: 4,
        borderWidth: 0.5,
        borderColor: '#d6d7da',
        backgroundColor:'rgba(255,255,255,0.3)',
        height:40,
        marginRight:5
    },
    input:{
        borderRadius: 4,
        borderWidth: 0.5,
        borderColor: '#d6d7da',
        backgroundColor:'rgba(255,255,255,0.3)',
        height:40
    },
    rowStyle:{
        marginTop: -25,
        padding:0
    },
    cardSelected:{
        backgroundColor:'rgba(255,255,255,1)'
    },
    cardNotSelected:{
        backgroundColor:'rgba(255,255,255,0.8)'
    },
    backgroundVideo: {
        flex:1,
        height:300
    },
    blurvideo:{
        flex:0,
        marginBottom:-300,
        height:300,
        opacity:0.5
    },
});
