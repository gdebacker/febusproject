import * as React from "react";
import {
  Container,
  Header,
  Title,
  Content,
  Text,
  Button,
  Icon,
  Left,
  Body,
  Right,
  List,
  ListItem,
} from "native-base";
import { Col, Row, Grid } from "react-native-easy-grid";
import LinearGradient from 'react-native-linear-gradient'
//import styles from "./styles";
import {Dimensions, View, StyleSheet, AsyncStorage, Image} from "react-native";
import SwitchButton from "../../../theme/components/SwitchButton";
export interface Props {
  navigation: any;
  list: any;
}
export interface State {}

class ChooseHowToStartYourCareer extends React.Component<Props, State> {

    constructor(props){
        super(props);
        try {
            AsyncStorage.getItem('account')
                .then(json => {
                    const user = JSON.parse(json);
                    console.log("***************** USER FROM ASYNC STORAGE (loginContainer) *******************");
                    console.log(user);
                    if(user != null) {
                        if (user.username !== null) {
                            this.props.navigation.navigate("HomeProfile");
                        }
                    }
                })
        } catch (error) {
            console.log("******************error getting user (loginContainer)********************");
            console.log(error);
        }
    }

    componentWillMount() {

    }

  render() {
    return (
        <View style={{flex:1}}>

            <View style={componentStyle1.container}>

                <View style={componentStyle1.container1}>
                    <LinearGradient colors={['#801B89', '#4151AF']} style={{flex:1, alignItems:'center' }}>
                        <Image
                            style={[styles.image,{marginTop:100}]}
                            source={require('./febus.png')}
                            resizeMode="stretch"
                        />
                        <Button
                            style={{backgroundColor:'#801B89', width:ScreenWidth - 60, alignSelf:'center', marginTop:30}}
                            block
                            onPress={() => this.props.navigation.navigate("ChooseHowToStartYourCareer")}>
                            <Text>Facebook</Text>
                        </Button>
                        <Button
                            style={{backgroundColor:'#801B89', width:ScreenWidth - 60, alignSelf:'center', marginTop:30}}
                            block
                            onPress={() => this.props.navigation.navigate("ChooseHowToStartYourCareer")}>
                            <Text>Google</Text>
                        </Button>
                        <Button
                            style={{backgroundColor:'#801B89', width:ScreenWidth - 60, alignSelf:'center', marginTop:30}}
                            block
                            onPress={() => this.props.navigation.navigate("Login")}>
                            <Text>Crée ton propre parcours ?</Text>
                        </Button>
                        <View style={{marginTop:40}}>
                        <SwitchButton
                            onValueChange={(val) => this.setState({ activeSwitch: val })}      // this is necessary for this component
                            text1 = 'FR'                        // optional: first text in switch button --- default ON
                            text2 = 'EN'                       // optional: second text in switch button --- default OFF
                            btnBackgroundColor = 'orange'
                            btnBorderColor = 'orange'
                        />
                        </View>
                    </LinearGradient>
                </View>



            </View>

        </View>
    );
  }
}

export default ChooseHowToStartYourCareer;

let ScreenHeight = Dimensions.get("window").height;
let ScreenWidth = Dimensions.get("window").width;

const componentStyle1 = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column'
    },
    container1: {
        flex: 1,
        flexDirection: 'row',
        backgroundColor: 'blue'
    },
    container2: {
        flex: 1,
        flexDirection: 'row',
        backgroundColor: 'green',
    },
})

const styles = StyleSheet.create({
    image: {
        alignSelf: 'center',
        height: 100,
        width: 100
    },
    fullHeight: {
        height: ScreenHeight-25,
        padding:10
    },
    textColor:{
        color:'white'
    },
    label:{
        color:'white',
        padding:10,
    },
    labelHalf:{
        color:'white',
        padding:10,
        flex:0.5,
        alignSelf:'flex-start'
    },
    inputHalf:{
        borderRadius: 4,
        borderWidth: 0.5,
        borderColor: '#d6d7da',
        backgroundColor:'rgba(255,255,255,0.3)',
        height:40,
        marginRight:5
    },
    input:{
        borderRadius: 4,
        borderWidth: 0.5,
        borderColor: '#d6d7da',
        backgroundColor:'rgba(255,255,255,0.3)',
        height:40
    },
    rowStyle:{
        marginTop: -25,
        padding:0
    },
    cardSelected:{
        backgroundColor:'rgba(255,255,255,1)'
    },
    cardNotSelected:{
        backgroundColor:'rgba(255,255,255,0.8)'
    }
});
